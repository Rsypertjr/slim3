<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\Query\Builder;

class ApiController
{
    protected $agents;
	protected $customers;
	protected $orders;

    public function __construct(
		Builder $agents,
		Builder $customers,
		Builder $orders
		)
    {
        $this->agents = $agents;
		$this->customers = $customers;
		$this->orders = $orders;
    }

    public function getAgents(Request $request, Response $response)
    {	
		
       // $set = array($this->agents->get(), $this->customers->get(), $this->orders->get());
	   $agentsArr = $this->agents->get();
		echo json_encode($agentsArr);
    }
	
	 public function getCustomers(Request $request, Response $response,$args)
    {	
	    //echo $args['agentCode'];
		$agentCode = $args['agentCode'];
		
		if($agentCode != null) {
			//print_r(json_encode($this->customers));
			echo json_encode($this->customers->where('AGENT_CODE',$agentCode)->get());
		}
		else{
			echo json_encode($this->customers);
			}
			
	}
	
	 public function getOrders(Request $request, Response $response,$args)
    {	
	    //echo $args['agentCode'];
		$agentCode = $args['agentCode'];
		
		if($agentCode != null) {
			//print_r(json_encode($this->customers));
			echo json_encode($this->orders->where('AGENT_CODE',$agentCode)->get());
		}
		else{
			echo json_encode($this->orders);
			}
			
	}
	 public function saveEdit(Request $request, Response $response,$args)
    {	
	    //echo $args['agentCode'];
		$type = $args['type'];
		
		if($type == 'agent') {
			 $agent = $request->getParsedBody();
			 $agent_code = $agent['AGENT_CODE'];
			 $agent_name = $agent['AGENT_NAME'];
			 
			 $result = $this->agents->where('AGENT_CODE',$agent_code)
									->update($agent);
			 if($result == 1){
				 $mess = "Agent ".preg_replace('/\s+/', '', $agent_name)." Updated Successfully!";
			     echo $mess;
			 }
			 else {
				 $mess = "Error Trying to Edit. Please Try Again";
			     echo $mess;
			 }
			//echo json_encode($request->getParsedBody()['AGENT_NAME']);
		}
		else if($type == 'customer') {
			 $customer = $request->getParsedBody();
			 $cust_code = $customer['CUST_CODE'];
			 $cust_name = $customer['CUST_NAME'];
			 
			 $result = $this->customers->where('CUST_CODE',$cust_code)
									->update($customer);
			 if($result == 1){
				 $mess = "Customer ".preg_replace('/\s+/', '', $cust_name)." Updated Successfully!";
			     echo $mess;
			 }
			 else {
				 $mess = "Error Trying to Edit. Please Try Again";
			     echo $mess;
			 }
			//echo json_encode($request->getParsedBody()['AGENT_NAME']);
		}
		else if($type == 'order') {
			 $order = $request->getParsedBody();
			 $ord_num = $order['ORD_NUM'];;
			 
			 $result = $this->orders->where('ORD_NUM',$ord_num)
									->update($order);
			 if($result == 1){
				 $mess = "Order ".preg_replace('/\s+/', '', $ord_num)." Updated Successfully!";
			     echo $mess;
			 }
			 else {
				 $mess = "Error Trying to Edit. Please Try Again";
			     echo $mess;
			 }
			//echo json_encode($request->getParsedBody()['AGENT_NAME']);
		}	
	}
	
	
	
}