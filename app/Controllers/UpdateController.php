<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class UpdateController
{
    protected $view;

    public function __construct(Twig $view)
    {
        $this->view = $view;
    }

    public function update(Request $request, Response $response)
    {
        return $this->view->render($response, 'update/update.html', ['statement' => 'Update Page']);
    }
}
