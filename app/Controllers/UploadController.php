<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use Slim\Http\UploadedFile;
use Slim\Container;
use GuzzleHttp\Client;
use Illuminate\Database\Query\Builder;

class UploadController
{
    protected $view;
	protected $c;
	protected $files;

    public function __construct(Container $c,Twig $view, Builder $files)
    {
        $this->view = $view;
		$this->c = $c;
		$this->files = $files;
    }

    public function upload(Request $request, Response $response)
    {
        return $this->view->render($response, 'upload/upload.html', ['statement' => 'Upload Page']);
    }
	
	 public function uploadS3(Request $request, Response $response)
    {
        return $this->view->render($response, 'upload/uploadS3.html', ['statement' => 'Upload Page']);
    }
	
	
	public function ams3(Request $request, Response $response)
    {
        return $this->view->render($response, 's3/amS3.php', []);
    }
	
	public function store(Request $request, Response $response)
    {
        $base_url = 'http://178.128.185.245/stv/public/';
		$statement = '';
		//$container = $this->app->getContainer();
		$container = $this->c;
		$container['upload_directory'] = __DIR__ . '/uploads';
		//$container['upload_directory'] =  './uploads';
		$directory = $container->get('upload_directory');
		$uploadedFiles = $request->getUploadedFiles();
		$filenames = array('');
		// handle single input with single file upload
		$uploadedFile = $uploadedFiles['example1'];
		
		
		if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
			//array_push($filenames,$this->moveUploadedFile($directory, $uploadedFile));
			
		    $filenames = $this->getFileStream($uploadedFile,$filenames,$statement);
    
		}

        
		// handle multiple inputs with the same key
		foreach ($uploadedFiles['example2'] as $uploadedFile) {
			if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
				//array_push($filenames,$this->moveUploadedFile($directory, $uploadedFile));
				$filenames = $this->getFileStream($uploadedFile,$filenames,$statement);
				
			}
		}

		// handle single input with multiple file uploads
		foreach ($uploadedFiles['example3'] as $uploadedFile) {
			if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
				//array_push($filenames,$this->moveUploadedFile($directory, $uploadedFile));
				$filenames = $this->getFileStream($uploadedFile,$filenames,$statement);
			}
		}
    
		 return $this->view->render($response, 'update/update.html', ['statement' => 'Update Page','filenames' => $filenames]);
	}
	

	
	
	
	
		/**
	 * Moves the uploaded file to the upload directory and assigns it a unique name
	 * to avoid overwriting an existing uploaded file.
	 *
	 * @param string $directory directory to which the file is moved
	 * @param UploadedFile $uploaded file uploaded file to move
	 * @return string filename of moved file
	 */
	public function moveUploadedFile($directory, UploadedFile $uploadedFile)
	{
		$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
		$basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
		$filename = sprintf('%s.%0.8s', $basename, $extension);

		$uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

		return $filename;
	}

	public function getFileStream($uploadedFile,$filenames,$statement){
		
			 $data = base64_encode($uploadedFile->getStream());
			 $fname = $uploadedFile->getClientFilename();
			
			 $check = $this->files->where('name',$fname)
			 					   ->count();	
			 if($check == 0){
				$result = $this->files->insert(['name' => $fname, 'content' => $data, 'date' => date('Y-m-d H:i:s')]);
				$statement = $statement."\nInsert Successful!";
				array_push($filenames,$fname);
			 }
			 else if($check > 0){
				$result = $this->files->where('name','=',$fname)
				                      ->update(['content' => $data, 'date' => date('Y-m-d H:i:s')]);
				$statement = $statement."\nUpdate Successful!";
				array_push($filenames,$fname);
			 }
			 else ;
			 
			 return $filenames;
	
	}
	
	
	
	
}
