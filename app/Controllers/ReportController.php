<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
class ReportController
{
    protected $view;
	
    public function __construct(Twig $view)
    {
        $this->view = $view; 
		
    }

    public function report(Request $request, Response $response, $args)
    {
		return $this->view->render($response, 'report/report.php', ['statement' => 'Report Page']);
    }
}