<?php

$app->get('/', 'HomeController:show');
$app->get('/update', 'UpdateController:update');
$app->get('/report', 'ReportController:report');

$app->get('/home', 'HomeController:show');
$app->get('/agents', 'ApiController:getAgents');
$app->get('/customers/{agentCode}', 'ApiController:getCustomers');
$app->get('/orders/{agentCode}', 'ApiController:getOrders');
$app->post('/save/{type}', 'ApiController:saveEdit');
$app->get('/uploads','UploadController:upload');

$app->get('/uploadsS3','UploadController:uploadS3');
$app->get('/ams3','UploadController:ams3');
$app->post('/store','UploadController:store');

$app->post('/storeS3','UploadController:storeS3');
