<?php

$settings = require __DIR__ . '/settings.php';

$app = new \Slim\App($settings);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager();
$capsule->addConnection($container['connections'][$container['default']]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['view'] = function ($c) {
    $settings = $c->get('settings')['view'];
    $view = new \Slim\Views\Twig($settings['path'], [
        //'cache' => $settings['cache'],
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $c->router,
        $c->request->getUri()
    ));

    return $view;
};

$container['HomeController'] = function ($c) {
    return new \App\Controllers\HomeController($c->view);
};

$container['ReportController'] = function ($c) {
	
    return new \App\Controllers\ReportController($c->view);
};

$container['UpdateController'] = function ($c) {
    return new \App\Controllers\UpdateController($c->view);
};

$container['UploadController'] = function ($c) {
	$files = $c->db->table('files');
    return new \App\Controllers\UploadController($c,$c->view,$files);
};
$container['VueController'] = function ($c) {
    return new \App\Controllers\VueController($c->view);
};
$container['ApiController'] = function ($c) {	
	$agents = $c->db->table('agents')->orderBy('AGENT_NAME','ASC');
	$customers = $c->db->table('customers')->orderBy('CUST_NAME','ASC');
	//print_r($customers->get());
	$orders = $c->db->table('orders')->orderBy('ORD_NUM','ASC');
    return new \App\Controllers\ApiController($agents,$customers,$orders);
};

$container['db'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};


$container['dbh'] = function($container) {

        $config = $container->get('settings')['pdo'];
        $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
        $username = $config['username'];
        $password = $config['password'];

        return new PDO($dsn, $username, $password, $config['options']);

}; 
