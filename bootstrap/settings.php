<?php

return [
    'settings' => [
        'displayErrorDetails' => true,
		 'addContentLengthHeader' => false,
        'view' => [
            'path' => __DIR__ . '/../resources/views',
            'cache' => __DIR__ . '/../resources/cache',
        ],
		'db' => [
            'driver' => 'pgsql',
            'host' => '178.128.185.245',
            'database' => 'ao_test',
            'username' => 'postgres',
            'password' => 'data',
            'charset' => 'utf8',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ],
		 'pdo' => [
            'engine' => 'mysql',
            'host' => '178.128.185.245',
            'database' => 'ao_test',
            'username' => '',
            'password' => 'syp3rt',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',

            'options' => [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES   => true,
                ],
        ],
    ],
    'default' => 'development',
    'connections' => [
        'development' => [
            'driver' => 'mysql',
            'host' => '178.128.185.245',
            'database' => 'ao_test',
            'username' => 'postgres',
            'password' => 'data',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ],
        'production' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => '',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ],
    ],
];
