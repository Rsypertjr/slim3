<html>
<head>
    <meta charset="UTF-8">
    <title>Report</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.4/vue.js"></script>
    
	<style>
		.nobord {position:relative;border:none;width:100%}
		.bord {position:relative;width:100%}
		 button.btn-warning {margin:0.5em 0 0 0}
		 #ordTable  button.btn-warning {margin:0 0 0 0}
		 [v-cloak] {
				display: none;
			  }
			  
		 #tableContainer   {
			 	height:auto;
				padding:1em;
				-webkit-transition: height 2s; /* Safari */
				transition: height 2s;
		 }
		 
	</style>

</head>
<body>

<div id="app">
    <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="#">Analytical Owl Assignment</a>
		</div>
		<ul class="nav navbar-nav">
		  <li class="active"><a href="/report">Report Page of PostgresSql Database</a></li>
		  <li><a href="/">Home Page</a></li>
		  <li><a href="/uploads">File Upload Page</a></li>		  
		  <li><a href="https://bitbucket.org/Rsypertjr/slim3/src/master/">Site Git Repo</a></li>
		</ul>
	  </div>
	</nav>
	<div v-cloak class="container">
		<div class="jumbotron" style="background-color:lightGrey;margin:2em;border:ridge darkGrey 1.0em">
		  <h1 class="display-4">Report Page</h1>
		  <hr class="my-4">
		  <p class="lead">Uses Vue Javascript and PostgresSQL used together to show tables representing Relational Data.  Vue does not externally manipulate 
					the DOM elements, but instead allows DOM elemental structure to be responsive to changes in data.  A Vue controller manipulates DOM content internally and 
					interacts with Slim PHP Controllers to get and send data thru an API to the PostgresSQL database.  Twig is also used for PHP Templating of HTML data.
		  </p>
		  <hr class="my-4">
		  <p>Click on See Agents to start viewing data.</p>		  
		</div>
		<div id="agents">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
					<div class="col-md-12">		
							<button style="margin:1em 0;zoom:150%" v-on:click="seeTheAgents(event,seeAgents,seeACustomers)" class="btn btn-primary btn-block">Click Here to See Agents Below and Scroll Down&nbsp;<span class="glyphicon glyphicon-arrow-down">&nbsp;</button>
					</div>
				</div>
				
			</div>
		</div>	
     </div>
    	 
	 <div id="tableContainer" v-cloak class="container">	
		<table v-if="seeAgents" class="table table-bordered table-striped">	
		    <caption style="width:100%;text-align:center;font-size:2em">List of Agents</caption>
			<thead>
				<th>Agent Name</th>
				<th>Working Area</th>
				<th>Commission</th>
				<th>Phone Number</th>
				<th style="text-align:center">Options</th>
			</thead>
			<tbody>
				<form>
					<template v-for="agent in agents" >							
						<tr>
							<td><input type="text" v-model="agent.AGENT_NAME" v-bind:class="{ nobord: isActive, bord : !isActive }" :disabled="inputDisabled"></td>
							<td><input type="text" v-model="agent.WORKING_AREA" v-bind:class="{ nobord: isActive, bord : !isActive  }" :disabled="inputDisabled"></td>
							<td><input type="text" v-model="agent.COMMISSION" v-bind:class="{ nobord: isActive, bord : !isActive  }"  :disabled="inputDisabled"></td>
							<td><input type="text" v-model="agent.PHONE_NO" v-bind:class="{ nobord: isActive, bord : !isActive  }"  :disabled="inputDisabled"></td>
							<td style="padding-left:2em">
							    <button class="btn btn-info" v-on:click="getACustomers(event,seeAgents,seeACustomers,agent.AGENT_CODE,agent.AGENT_NAME)">Customers</button>
								<button class="btn btn-info" v-on:click="getAOrders(event,seeAgents,seeACustomers,seeAOrders,agent.AGENT_CODE,agent.AGENT_NAME)">Orders</button>
								<button class="btn btn-success" v-if="!doEdit" v-on:click="editvalue(event)"><span class="glyphicon glyphicon-edit"></span>Edit</button> 
								<button class="btn btn-success" v-if="doEdit" v-on:click="savevalue(event,agent,'agent')"><span class="glyphicon glyphicon-edit"></span>Save</button>
								<button class="btn btn-warning" v-if="doEdit" v-on:click="cancel(event)"><span class="glyphicon glyphicon-edit"></span>Cancel</button>  
							</td>
						</tr>
					</template>		
				</form>	
			</tbody>
		</table>		
					
					
		<table v-if="seeACustomers"  class="table table-bordered table-striped">
					<caption style="width:100%;text-align:center;font-size:2em" v-text="'Customers of agent ' + agName"></caption>
					<thead>
					    <th>Agent Code</th>
						<th>Customer Name</th>
						<th>Customer City</th>
						<th>Working Area</th>
						<th>Customer Country</th>
						<th>Grade</th>
						<th>Opening Amount</th>
						<th>Recieve Amount</th>
						<th>Payment Amount</th>
						<th>Outstanding Amount</th>
						<th>Phone Number</th>
						<th>Options</th>
					</thead>
					<tbody>
						<form enctype="multipart/form-data">
							<template v-for="customer in customers">
							  <tr>
									<td><input type="text" v-model="customer.AGENT_CODE" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.CUST_NAME" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>								
									<td><input type="text" v-model="customer.CUST_CITY" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.WORKING_AREA" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.CUST_COUNTRY"  v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.GRADE" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.OPENING_AMT" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.RECEIVE_AMT" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.PAYMENT_AMT" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.OUTSTANDING_AMT" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td><input type="text" v-model="customer.PHONE_NO" v-bind:class="{ nobord: isActive, bord : !isActive }"  :disabled="inputDisabled"></td>
									<td>
										<button class="btn btn-success" v-if="!doEdit" v-on:click="editvalue(event)"><span class="glyphicon glyphicon-edit"></span>Edit</button>										
								        <button class="btn btn-success" v-if="doEdit" v-on:click="savevalue(event,customer,'customer')"><span class="glyphicon glyphicon-edit"></span>Save</button> 
										<button class="btn btn-warning" v-if="doEdit" v-on:click="cancel(event)"><span class="glyphicon glyphicon-edit"></span>Cancel</button> 
									</td>									
							   </tr>
							</template>
						</form>
					</tbody>
		</table>
			
						 
		<table id="ordTable" v-if="seeAOrders" class="table table-bordered table-striped">
					<caption style="width:100%;text-align:center;font-size:2em" v-text="'Orders of agent ' + agName"></caption>
					<thead>
						<th>Order Number</th>
						<th>Order Amount</th>
						<th>Advance Amount</th>
						<th>Order Date</th>	
						<th>Options</th>
					</thead>
					<tbody>
					  <form>
						<template v-for="order in orders">
							<tr>
									<td><input type="text" v-model="order.ORD_NUM" :disabled="inputDisabled"></td>
									<td><input type="text" v-model="order.ORD_AMOUNT" :disabled="inputDisabled"></td>
									<td><input type="text" v-model="order.ADVANCE_AMOUNT" :disabled="inputDisabled"></td>
									<td><input type="text" v-model="order.ORD_DATE" :disabled="inputDisabled"></td>
									<td>
										<button class="btn btn-success" v-if="!doEidt" v-on:click="editvalue(event)" ><span class="glyphicon glyphicon-edit"></span>Edit</button>																			
								        <button class="btn btn-success" v-if="doEdit" v-on:click="savevalue(event,order,'order')"><span class="glyphicon glyphicon-edit"></span>Save</button> 
										<button class="btn btn-warning" v-if="doEdit" v-on:click="cancel(event)"><span class="glyphicon glyphicon-edit"></span>Cancel</button> 
			                         </td>
							</tr>
						  </template>
					   </form>
					 </tbody>
		</table>
						
	</div>
</div>
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>	
//Vue.use(require('vue-chartist'));

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
 var base_url = '/';
	 var url = base_url+'agents';
	 //alert(url);
	 this.vue =  new Vue({
			el: '#app',
			props: ['seeAgents','seeACustomers','seeAOrders','agents','customers','orders','agName','agCode','inputDisabled','isActive','doEdit'],
			data: function(){
				return	{
					agents: [],
					customers: [],
					orders: [],
					seeAgents : false,
					seeACustomers : false,
					seeAOrders : false,
					disabled:true,
					inputDisabled : false,
					isActive :true,
					doEdit :false,
					agName : '',
					agCode : ''
				}
			},
		
		methods: {
				seeTheAgents: function(event,seeAgents,seeACustomers) {
						this.seeAgents = true;
						this.seeACustomers = false;
						this.seeAOrders = false;
						this.inputDisabled = true;
						this.isActive = true;
						this.doEdit = false;						
			            
				},
				
			   getACustomers: function(event,seeAgents,seeACustomers,agentCode,agentName){
				  var url = base_url+'customers/'+agentCode.toString();
				  this.seeAgents = false;			  
				  this.seeACustomers = true;
				  this.seeAOrders = false;
				  this.agName = agentName;
				  axios
					.get(url)
					.then(response => {
						this.customers = response.data					
					})	
				},
				
				getAOrders: function(event,seeAgents,seeACustomers,seeAOrders,agentCode,agentName){
				  var url = base_url+'orders/'+agentCode.toString();
				  this.seeAgents = false;			  
				  this.seeACustomers = false;
				  this.seeAOrders = true;
				  this.agName = agentName;
				  axios
					.get(url)
					.then(response => {
						this.orders = response.data					
					})	
				},
			 
			  getAgentsCustomers: function(event,agentCode){
				  var url = base_url+'customers/'+agentCode;
				  //alert(url);
				  axios
					.get(url)
					.then(response => {
						this.customers = response.data					
					})	
				},
			 
			  getOrders: function(event){
				  var url = base_url+'orders';
				  //alert(url);
				  axios
					.get(url)
					.then(response => {
						this.orders = response.data					
					})	
				 },
			  editvalue: function(event){
					this.inputDisabled = !this.inputDisabled;
					this.isActive = !this.isActive;
					this.doEdit = !this.doEdit;
				 }, 

			  savevalue: function(event,object,type){
				    var url = base_url+'save/'+type;
					this.doEdit = false;
					this.inputDisabled = !this.inputDisabled;
					this.isActive = !this.isActive;				
					
					axios
					.post(url,object)
					.then(response => {
						alert(response.data)					
					})	
					
				 },  
             cancel: function(event){					
					this.doEdit = false;
					this.inputDisabled = !this.inputDisabled;
					this.isActive = !this.isActive;
				 } 					 
			},
		   mounted: function() {
			  axios
				.get(url)
				.then(response => {
					this.agents = response.data					
				})	
			}  			
		});
</script>
	
	
	
</body>
</html>